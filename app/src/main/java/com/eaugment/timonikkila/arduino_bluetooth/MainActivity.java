package com.eaugment.timonikkila.arduino_bluetooth;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.nio.charset.StandardCharsets;
import java.util.Set;
import java.util.UUID;



public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final int REQUEST_ENABLE_BT = 1513;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            ConnectThread thr;
            MyBluetoothService serv = null;

            @Override
            public void onClick(View view) {


                BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if (mBluetoothAdapter == null) {
                    System.out.println("ei blue thoottia");
                    // Device doesn't support Bluetooth
                } else {
                    System.out.println("on blue thoottia");
                }

                if (!mBluetoothAdapter.isEnabled()) {
                    System.out.println("ei ole enab");
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                } else {
                    System.out.println("on enab");
                }

                Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

                if (pairedDevices.size() > 0) {
                    // There are paired devices. Get the name and address of each paired device.
                    for (BluetoothDevice device : pairedDevices) {
                        String deviceName = device.getName();
                        String deviceHardwareAddress = device.getAddress(); // MAC address
                        if (deviceName.equals("H-C-2010-06-01")) {

                            //jos serv on socket, niin käytä vanhaa.
                            if (serv == null || serv.getSocket() == null) {

                                thr = new ConnectThread(device, mBluetoothAdapter);
                                serv = new MyBluetoothService();


                                @SuppressLint("HandlerLeak") Handler mHandler = new Handler() {
                                    @Override
                                    public void handleMessage(Message msg) {
                                        super.handleMessage(msg);
                                        System.out.println(msg.arg1);
                                        System.out.println(msg.what);
                                        if (msg.what == CustomMessages.SOCKET_READY) {
                                            System.out.println("ja taas toimii");
                                            serv.setSocket(thr.getSocket());
                                            serv.startCommunication();
                                        } else if (msg.what == CustomMessages.CANCEL) {
                                            System.out.println("soketti suljettu");
                                            thr.cancel();
                                            serv.cancel();

                                            serv = null;
                                        }


                                    }
                                };


                                // luo handler ja anna se ConnectThread parametriksi.
                                // jos handler what == socket_ready niin kutsu btserviceä. käytä handler.arg1 == socket
                                // jos handler what == received_message niin kutsu bt_serice.write, handler.arg1 == message
                                thr.setHandler(mHandler);
                                serv.setHandler(mHandler);

                                thr.start();
                                System.out.println("oleva laite " + deviceName + " ---- " + deviceHardwareAddress);
                            } else {
                                System.out.println("käyttää valmista sokettia");
                                serv.startCommunication();
                            }

                        }
                    }
                }

                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
